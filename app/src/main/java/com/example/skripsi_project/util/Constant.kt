package com.example.skripsi_project.util

const val LOVE = "Love"
const val HELLO = "Hello"
const val DATABASE_NAME = "mPolicy.db"
const val OLD_BASE_URL = "https://epolicy.sinarmasmsiglife.co.id"
const val BASE_URL = "http://paymentcc.sinarmasmsiglife.co.id:8082/API-Policy/"
const val TMDB_API_KEY = "6f9eb82919b6afb48a95af0c8c16dfac"
const val REQUEST_IMAGE_CAPTURE = 1
const val PREF_NAME = "mPolicy_data"
const val USERNAME = "username"
const val USER_LOGIN_KEY = "loginKey"
const val PREF_KEY_LOGIN = "isLogin"
const val SELECTED_POLICY_NUMBER = "selectedNoPolis"
const val SELECTED_PEMEGANG_POLIS = "selectedPP"
const val CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
const val READ_TIMEOUT = "READ_TIMEOUT"
const val WRITE_TIMEOUT = "WRITE_TIMEOUT"
const val DEFAULT_CONNECT_TIMEOUT: Long = 60000
const val DEFAULT_READ_TIMEOUT: Long = 60000
const val DEFAULT_WRITE_TIMEOUT: Long = 60000
const val UNIT_LINK = "ringkasan link"
const val POWERSAVE = "Powersave"

