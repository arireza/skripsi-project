package com.example.skripsi_project.util.handlers

import android.view.View

interface SkripsiClickHandlers {
    interface AddProduct {
        fun onButtonSaveProductPressed(view : View)
    }
    interface ProductList {
        fun onFabAddProductPressed(view : View)
    }

}