package com.example.skripsi_project.di.module

import com.example.skripsi_project.ui.admin.home.AdminHomeFragment
import com.example.skripsi_project.ui.login.LoginFragment
import com.example.skripsi_project.ui.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributesSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributesAdminHomeFragment(): AdminHomeFragment

    /*@ContributesAndroidInjector
    abstract fun contributesPagerFragment(): PagerFragment

    @ContributesAndroidInjector
    abstract fun contributesPemegangPolisFragment(): PemegangPolisFragment

    @ContributesAndroidInjector
    abstract fun contributesTertanggungFragment(): TertanggungFragment

    @ContributesAndroidInjector
    abstract fun contributesDataAsuransiFragment(): DataAsuransiFragment

    @ContributesAndroidInjector
    abstract fun contributesIstilahFragment(): IstilahFragment

    @ContributesAndroidInjector
    abstract fun contributesFaqFragment(): FAQFragment

    @ContributesAndroidInjector
    abstract fun contributesManfaatPolisFragment(): ManfaatPolisFragment

    @ContributesAndroidInjector
    abstract fun contributesStatusPembayaranFragment(): StatusPembayaranFragment

    @ContributesAndroidInjector
    abstract fun contributesInvestasiFragment(): InvestasiTransaksiFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailInvestasiFragment(): DetailInvestasiFragment

    @ContributesAndroidInjector
    abstract fun contributesRekeningFragment(): RekeningFragment

    @ContributesAndroidInjector
    abstract fun contributesNABFragment(): NABFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailNABFragment(): DetailNABFragment

    @ContributesAndroidInjector
    abstract fun contributesProviderSemuaFragment(): SemuaFragment

    @ContributesAndroidInjector
    abstract fun contributesProviderTerdekatFragment(): TerdekatFragment

    @ContributesAndroidInjector
    abstract fun contributesKontakKamiFragment(): KontakFragment

    @ContributesAndroidInjector
    abstract fun contributesProviderFavoriteFragment(): FavoritFragment*/
}