package com.example.skripsi_project.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.skripsi_project.di.ViewModelFactory
import com.example.skripsi_project.di.ViewModelKey
import com.example.skripsi_project.ui.admin.home.AdminHomeViewModel
import com.example.skripsi_project.ui.login.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory : ViewModelFactory) : ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun providesLoginViewModel(viewModel : LoginViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AdminHomeViewModel::class)
    internal abstract fun providesHomeViewModel(viewModel : AdminHomeViewModel) : ViewModel

    /*@Binds
    @IntoMap
    @ViewModelKey(PagerViewModel::class)
    internal abstract fun providesPagerViewModel(viewModel : PagerViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PemegangPolisViewModel::class)
    internal abstract fun providesPemegangPolisViewModel(viewModel : PemegangPolisViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TertanggungViewModel::class)
    internal abstract fun providesTertanggungViewModel(viewModel : TertanggungViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DataAsuransiViewModel::class)
    internal abstract fun providesDataAsuransiViewModel(viewModel : DataAsuransiViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManfaatPolisViewModel::class)
    internal abstract fun providesManfaatPolisViewModel(viewModel : ManfaatPolisViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StatusPembayaranViewModel::class)
    internal abstract fun providesStatusPembayaranViewModel(viewModel : StatusPembayaranViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InvestasiTransaksiViewModel::class)
    internal abstract fun providesInvestasiTransaksiViewModel(viewModel: InvestasiTransaksiViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailInvestasiViewModel::class)
    internal abstract fun providesDetailInvestasiTransaksiViewModel(viewModel : DetailInvestasiViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RekeningViewModel::class)
    internal abstract fun providesRekeningViewModel(viewModel : RekeningViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NABViewModel::class)
    internal abstract fun providesNABViewModel(viewModel : NABViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailNABViewModel::class)
    internal abstract fun providesDetailNABViewModel(viewModel : DetailNABViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProviderSemuaViewModel::class)
    internal abstract fun providesProviderSemuaViewModel(viewModel : ProviderSemuaViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProviderTerdekatViewModel::class)
    internal abstract fun providesProviderTerdekatViewModel(viewModel : ProviderTerdekatViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProviderFavoriteViewModel::class)
    internal abstract fun providesProviderFavoriteViewModel(viewModel : ProviderFavoriteViewModel) : ViewModel*/
}