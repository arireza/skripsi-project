package com.example.skripsi_project.di.module

import dagger.Module

@Module
class RepositoryModule {
  /*  @Provides
    @Singleton
    fun provideListPolisDao(db: mPolicyDatabase) = db.listPolisDao()

    @Provides
    @Singleton
    fun providePemegangPolisDao(db: mPolicyDatabase) = db.pemegangPolisDao()

    @Provides
    @Singleton
    fun provideTertanggungDao(db: mPolicyDatabase) = db.tertanggungDao()

    @Provides
    @Singleton
    fun provideDataAsuransiDao(db: mPolicyDatabase) = db.dataAsuransiDao()

    @Provides
    @Singleton
    fun providerStatusPembayaranDao(db: mPolicyDatabase) = db.statusPembayaranDao()

    @Provides
    @Singleton
    fun provideRekeningDao(db: mPolicyDatabase) = db.rekeningDao()

    @Provides
    @Singleton
    fun provideProviderDao(db: mPolicyDatabase) = db.providersDao()

    @Provides
    @Singleton
    fun provideLoginRepository(remoteDataSource: LoginRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = LoginRepository(remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideHomeRepository(localDataSource: HomeLocalDataSource, remoteDataSource: HomeRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = HomeRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun providePagerRepository(remoteDataSource: PagerRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = PagerRepository(remoteDataSource, netManager, editor, preferences)


    @Provides
    @Singleton
    fun providePemegangPolisRepository(localDataSource: PemegangPolisLocalDataSource, remoteDataSource: PemegangPolisRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = PemegangPolisRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideTertanggungRepository(localDataSource: TertanggungLocalDataSource, remoteDataSource: TertanggungRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = TertanggungRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideRekeningRepository(localDataSource: RekeningLocalDataSource, remoteDataSource: RekeningRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = RekeningRepository(localDataSource, remoteDataSource, netManager, editor, preferences)


    @Provides
    @Singleton
    fun provideDataAsuransiRepository(localDataSource: DataAsuransiLocalDataSource, remoteDataSource: DataAsuransiRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = DataAsuransiRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideManfaatPolisRepository(remoteDataSource: ManfaatPolisRemoteDataSource) = ManfaatPolisRepository(remoteDataSource)

    @Provides
    @Singleton
    fun provideStatusPembayaranRepository(localDataSource: StatusPembayaranLocalDataSource, remoteDataSource: StatusPembayaranRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = StatusPembayaranRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideInvestasiTransaksiRepository(remoteDataSource: InvestasiTransaksiRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = InvestasiTransaksiRepository(remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideNABRepository(remoteDataSource: NABRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = NABRepository(remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideDetailNABRepository(remoteDataSource: DetailNABRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = DetailNABRepository(remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideDetailInvestasiTransaksiRepository(remoteDataSource: DetailInvestasiTransaksiRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = DetailInvestasiTransaksiRepository(remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideProviderSemuaRepository(localDataSource: ProviderLocalDataSource, remoteDataSource: ProviderSemuaRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = ProviderSemuaRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideProviderTerdekatRepository(localDataSource: ProviderTerdekatLocalDataSource, remoteDataSource: ProviderTerdekatRemoteDataSource, netManager: NetManager, editor: SharedPreferences.Editor, preferences: SharedPreferences) = ProviderTerdekatRepository(localDataSource, remoteDataSource, netManager, editor, preferences)

    @Provides
    @Singleton
    fun provideProviderFavoriteRepository(localDataSource: ProviderFavoriteLocalDataSource, editor: SharedPreferences.Editor, preferences: SharedPreferences) = ProviderFavoriteRepository(localDataSource, editor, preferences)*/
}