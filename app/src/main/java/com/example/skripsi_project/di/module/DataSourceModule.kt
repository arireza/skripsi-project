package com.example.skripsi_project.di.module

import dagger.Module

@Module
class DataSourceModule {
   /* @Provides
    @Singleton
    fun providesLoginRemoteDataSource(apiService : ApiService) = LoginRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesListProviderLocalDataSource(listProviderDao : ProvidersDao) = ProviderLocalDataSource(listProviderDao)

    @Provides
    @Singleton
    fun providesProviderFavoriteLocalDataSource(listProviderDao: ProvidersDao) = ProviderFavoriteLocalDataSource(listProviderDao)

    @Provides
    @Singleton
    fun providesProviderTerdekatLocalDataSource(listProviderDao: ProvidersDao) = ProviderTerdekatLocalDataSource(listProviderDao)

    @Provides
    @Singleton
    fun providesHomeRemoteDataSource(apiService : ApiService) = HomeRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesHomeLocalDataSource(listPolisDao : ListPolisDao) = HomeLocalDataSource(listPolisDao)

    @Provides
    @Singleton
    fun providesPemegangPolisRemoteDataSource(apiService : ApiService) = PemegangPolisRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesPemegangPolisLocalDataSource(pemegangPolisDao : PemegangPolisDao) = PemegangPolisLocalDataSource(pemegangPolisDao)

    @Provides
    @Singleton
    fun providesDataAsuransiRemoteDataSource(apiService : ApiService) = DataAsuransiRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesDataAsuransiLocalDataSource(dataAsuransiDao: DataAsuransiDao) = DataAsuransiLocalDataSource(dataAsuransiDao)

        @Provides
    @Singleton
    fun providesTertanggungRemoteDataSource(apiService : ApiService) = TertanggungRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesTertanggungLocalDataSource(tertanggungDao: TertanggungDao) = TertanggungLocalDataSource(tertanggungDao)

    @Provides
    @Singleton
    fun providesManfaatPolisRemoteDataSource(apiService : ApiService) = ManfaatPolisRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesRekeningDataSource(apiService : ApiService) = RekeningRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesRekeningLocalDataSource(rekeningDao: RekeningDao) = RekeningLocalDataSource(rekeningDao)

    @Provides
    @Singleton
    fun providesStatusPembayaranRemoteDataSource(apiService : ApiService) = StatusPembayaranRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesStatusPembayaranLocalDataSource(statuspembayaranDao : StatusPembayaranDao) = StatusPembayaranLocalDataSource(statuspembayaranDao)

    @Provides
    @Singleton
    fun providesInvestasiTransaksiRemoteDataSource(apiService: ApiService) = InvestasiTransaksiRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesDetailInvestasiTransaksiRemoteDataSource(apiService : ApiService) = DetailInvestasiTransaksiRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesNABRemoteDataSource(apiService: ApiService) = NABRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesDetailNABRemoteDataSource(apiService: ApiService) = DetailNABRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesProviderSemuaRemoteDataSource(apiService : ApiService) = ProviderSemuaRemoteDataSource(apiService)

    @Provides
    @Singleton
    fun providesProviderTerdekatRemoteDataSource(apiService : ApiService) = ProviderTerdekatRemoteDataSource(apiService)*/
}