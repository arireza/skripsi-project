package com.example.skripsi_project.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.skripsi_project.SkripsiApp
import com.example.skripsi_project.di.Info
import com.example.skripsi_project.util.HELLO
import com.example.skripsi_project.util.LOVE
import com.example.skripsi_project.util.PREF_NAME
import com.example.skripsi_project.util.annotation.Use
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Use(LOVE)
    fun providesInfoLove() = Info("Love Dagger2")


    @Provides
    @Use(HELLO)
    fun providesInfoHello() = Info("Hello Dagger 2")


    @Provides
    @Singleton
    fun provideContext(app: SkripsiApp) : Context = app

    @Provides
    @Singleton
    fun provideApplications(app : SkripsiApp) : Application = app

   /* @Provides
    @Singleton
    fun provideDatabase(context: Context) = Room.databaseBuilder(context, mPolicyDatabase::class.java, DATABASE_NAME).build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)*/

    @Provides
    @Singleton
    fun providesPreference(app : SkripsiApp) : SharedPreferences  = app.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun providesSharedPreference(sharedPreferences: SharedPreferences) : SharedPreferences.Editor = sharedPreferences.edit()
}