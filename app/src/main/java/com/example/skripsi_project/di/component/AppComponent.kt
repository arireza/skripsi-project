package com.example.skripsi_project.di.component

import com.example.skripsi_project.SkripsiApp
import com.example.skripsi_project.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    DataSourceModule::class,
    AndroidInjectionModule::class,
    ActivityBuilder::class,
    FragmentModule::class,
    ViewModelModule::class])

interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: SkripsiApp): Builder

        fun build(): AppComponent
    }

    fun inject(app: SkripsiApp)
}
