package com.example.skripsi_project.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController

import com.example.skripsi_project.R
import com.example.skripsi_project.databinding.FragmentLoginBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class LoginFragment : Fragment() {
    private lateinit var viewModel: LoginViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentLoginBinding
    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)

        binding.apply {
            vm = viewModel
            binding.executePendingBindings()
        }
        binding.setLifecycleOwner(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottomNavigationView = activity!!.findViewById(R.id.bottomNavigationView)

        viewModel.isLoginSuccess.observe(this, Observer {
            if (true) {
                val action = LoginFragmentDirections.actionLaunchAdminHomeFragment()
                findNavController().navigate(action)
                bottomNavigationView.menu.clear()
                bottomNavigationView.inflateMenu(R.menu.menu_bottom_logged_in_admin)
            } else {
                Toast.makeText(activity, "Login gagal", Toast.LENGTH_SHORT).show()
            }
        })

    }

}
