package com.example.skripsi_project.ui.splash


import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.example.skripsi_project.R
import com.example.skripsi_project.ui.login.LoginViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class SplashFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var binding: com.example.skripsi_project.databinding.FragmentSplashBinding

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        binding.apply {
            vm = viewModel
            binding.executePendingBindings()
        }

        Handler().postDelayed({
            val action = SplashFragmentDirections.actionLaunchloginFragment()
            findNavController().navigate(action)
        }, 2000)
        return binding.root
    }
}
