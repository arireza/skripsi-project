package com.example.skripsi_project.ui.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField
import javax.inject.Inject

class LoginViewModel @Inject constructor() : ViewModel() {
    var username = ObservableField<String>("admin")
    var password = ObservableField<String>("admin")

    private val _isLoginSuccess = MutableLiveData<Boolean>()
    val isLoginSuccess: LiveData<Boolean>
        get() = _isLoginSuccess

    fun validateLogin() {
        val username = username.get()
        val password = password.get()

        if (username.isNullOrEmpty()) {
            _isLoginSuccess.value = false
        } else {
            if (password.isNullOrEmpty()) {
                _isLoginSuccess.value = false
            } else {
                doLogin(username!!, password)
            }
        }

    }

    private fun doLogin(username: String, password: String?) {
        _isLoginSuccess.value = true
    }
}
