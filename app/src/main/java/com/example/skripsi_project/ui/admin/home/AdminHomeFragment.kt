package com.example.skripsi_project.ui.admin.home

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.skripsi_project.R
import com.example.skripsi_project.databinding.FragmentAdminHomeBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class AdminHomeFragment : Fragment() {
    private lateinit var viewModel: AdminHomeViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentAdminHomeBinding

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AdminHomeViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_admin_home, container, false)

        binding.apply {
            vm = viewModel
            binding.executePendingBindings()
        }
        binding.setLifecycleOwner(this)

        return binding.root
    }
}
